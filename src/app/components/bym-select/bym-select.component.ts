import {
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

@Component({
  selector: 'bym-select',
  templateUrl: './bym-select.component.html',
  styleUrls: ['./bym-select.component.scss'],
})
export class BymSelectComponent {
  selectedBookTitleWithChapter: string | undefined;
  @Input() values: string[] | undefined;
  @Input() showIcon: boolean | undefined;
  @Output() valueSelected = new EventEmitter<string>();
  @Input() defaultValue: string | undefined;

  selectValue(event: any) {
    this.valueSelected.emit(event.value);
  }
}
