import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BymSelectComponent } from './bym-select.component';

describe('BymSelectComponent', () => {
  let component: BymSelectComponent;
  let fixture: ComponentFixture<BymSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BymSelectComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BymSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
