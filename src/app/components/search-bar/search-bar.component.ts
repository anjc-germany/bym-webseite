import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'bym-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent {
  searchText: string | undefined;
  @Output() searchTerm = new EventEmitter<string>() 

  searchChanged() {
    console.log(this.searchText);
    this.searchTerm.emit(this.searchText);
  }

  clearSearch() {
    this.searchText = '';
    this.searchTerm.emit(this.searchText)
  }
}
