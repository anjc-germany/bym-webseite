import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BymInputComponent } from './bym-input.component';

describe('BymInputComponent', () => {
  let component: BymInputComponent;
  let fixture: ComponentFixture<BymInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BymInputComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BymInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
