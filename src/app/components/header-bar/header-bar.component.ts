import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { BibleDataService } from 'src/app/shared/services/bible-data.service';
import { Book, Chapter, SelectItem } from 'src/app/shared/models/book';
import { BehaviorSubject, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, takeUntil, tap } from 'rxjs/operators';
import { ActivatedRoute, Params, Router, ActivationStart } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search.service';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'bym-header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.scss']
})
export class HeaderBarComponent implements OnInit, OnDestroy {
  @Output() toggleSidenav = new EventEmitter();
  _books: Book[] = [];
  @Input() 
  set books(books: Book[]) {
    this._books = books;
    this.triggerSelection();
  }

  get books() {
    return this._books;
  }
  @Input() versions: SelectItem[] | undefined;
  @Input() langs: SelectItem[] | undefined;
  chapterOsisId: string | undefined;
  unsubscribe = new Subject<void>();
  currentUrl: string | undefined;
  searchText: string | undefined;
  searchSubject$ = new BehaviorSubject<string>('');
  search$ = this.searchSubject$.asObservable();
  selectedBook: Book | undefined;
  selectedBookTitleWithChapter: string | undefined;
  defaultVersion = 'BYM-DE';
  defaultLang = 'DE';

  constructor(
    public bibleDataService: BibleDataService, 
    private route: ActivatedRoute, 
    private router: Router,
    private searchService: SearchService,
    private storageService: LocalStorageService,
    private translate: TranslateService
    ) {}

  ngOnInit(): void {
    this.triggerSelection();
    this.router.events
    .pipe(
      filter( event =>event instanceof ActivationStart)
    )
    .subscribe((event: any) => {
      this.currentUrl = event?.snapshot?.routeConfig?.path;
        console.log(event?.snapshot?.routeConfig);
    });

    this.bibleDataService.chapterOsisId$
      .pipe(
        takeUntil(this.unsubscribe)
      )
      .subscribe(osisId => {
        this.chapterOsisId = osisId;
        console.log(osisId);
        const chapterNr = osisId?.split('.')[1];
        if(chapterNr) {
          this.selectedBookTitleWithChapter = `${this.selectedBook?.title} ${chapterNr}`;
        }    
        this.storageService.setItem('osisId', this.chapterOsisId || '');
        this.updateQueryParams({osisId: osisId});
      });

      this.search$
        .pipe(
          debounceTime(500),
          tap(log => console.log(log)),
          distinctUntilChanged(),
          map(stext => this.searchService.search(stext)),
        )
        .subscribe((sresults) => {
          console.log(sresults);
          this.searchService.searchChanged(sresults)
        })
  }

  selectChapter(chapter: Chapter) {
    const queryParams = {osisId: chapter?.osisID};
    console.log(this.currentUrl);
    const chapterNr = chapter?.osisID?.split('.')[1];
    this.selectedBookTitleWithChapter = `${this.selectedBook?.title} ${chapterNr}`;
    localStorage.setItem('osisId', chapter?.osisID || '');
    this.bibleDataService.chapterSelected(chapter);
    if (this.currentUrl !== 'bible') {
      this.router.navigate(
        ['bible'], 
        {
          relativeTo: this.route,
          queryParams, 
          queryParamsHandling: 'merge', // remove to replace all query params by provided
        });
    } else {
      this.updateQueryParams(queryParams);
    }
  }

  updateQueryParams(queryParams: Params) {
    this.router.navigate(
      [], 
      {
        relativeTo: this.route,
        queryParams, 
        queryParamsHandling: 'merge', // remove to replace all query params by provided
      });
  }

  selectBook(book: Book) {
    this.selectedBook = book;
    this.bibleDataService.bookSelected(book);
  }

  selectVersion(name: string) {
    const version: any = this.versions?.find(version => version.name === name);
    this.bibleDataService.bibleVersionChanged(version?.lang);
  }

  selectLang(name: string) {
    const lang: any = this.langs?.find(l => l.name === name);
    this.translate.use(lang.lang);
  }

  versionsMapped() {
    return this.versions?.map(version => version.name)
  }

  langsMapped() {
    return this.langs?.map(version => version.name)
  }

  searchTextChanged(searchText: string) {
    if (searchText === "") {
      this.searchSubject$.next('');
    } else {
      this.searchSubject$.next(searchText?.trim());
    }
  }

  loadBookFromLSStorage() {
    const osisId = localStorage.getItem('osisId') || undefined;
    const bookOsisID = osisId?.split('.')[0];
    console.log('loadBookFromLSStorage', bookOsisID);
    
    const book = osisId ? this._books.find(book => book.osisID === bookOsisID) : null;

    return book;
  }

  triggerSelection(index: number = 0) {
    const osisId = localStorage.getItem('osisId') || undefined;
    const chapterNr: any = osisId?.split('.')[1];
    const book: any = (this.books && osisId) ? this.loadBookFromLSStorage(): this.books[index];
    const chapter: any = book?.chapters[(parseInt(chapterNr) - 1) || index];
    this.selectBook(book);
    this.selectChapter(chapter);    
  }

  ngOnDestroy(): void {
      this.unsubscribe.next();
      this.unsubscribe.complete();
  }  
}
