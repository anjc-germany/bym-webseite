import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { Book, Chapter } from 'src/app/shared/models/book';
import { MatMenuTrigger } from '@angular/material/menu';

@Component({
  selector: 'bym-select-book',
  templateUrl: './select-book.component.html',
  styleUrls: ['./select-book.component.scss']
})
export class SelectBookComponent {
  @ViewChild('menuChaptersTrigger') menuChaptersTrigger:
    | MatMenuTrigger
    | undefined;
  
  chapters: Chapter[] | undefined;
  _books: Book[] = [];
  @Input() selectedBook: Book | undefined;
  @Input() 
  set books(books: Book[]) {
    this._books = books; 
  }
  get books() {
    return this._books;
  }
  @Output() chapterSelected = new EventEmitter<Chapter>();
  @Output() bookSelected = new EventEmitter<Book>();
  _chapterOsisId: string | undefined;
  @Input() 
  set chapterOsisId(osisID: string | undefined) {
    this._chapterOsisId = osisID;
  };
  @Input()
  bookTitleWithChapterSelected: string | undefined;

  selectBook(book: Book | undefined, canOpen: boolean = true) {
    this.chapters = book?.chapters;
    this.bookSelected.emit(book);

    if (canOpen) {
      this.menuChaptersTrigger?.openMenu();
    }
  }

  selectChapter(chapter: Chapter) {
    this.chapterSelected.emit(chapter);
    localStorage.setItem('osisId', chapter?.osisID || '');
  }

  bookChapterChanged(bookChap: any) {
    const bookChapArr =  bookChap.split(' ');
    if(bookChapArr.length > 1) {
      const chapNr = bookChapArr.length > 2 ? parseInt(bookChapArr[bookChapArr.length - 1]) : parseInt(bookChapArr[1]);
      if (this.selectedBook?.chapters && chapNr <= this.selectedBook?.chapters.length) {
        const chapter = this.selectedBook?.chapters[chapNr - 1];
        this.chapterSelected.emit(chapter);
        console.log('bookChapterChanged', chapter);
        // localStorage.setItem('osisId', chapter?.osisID || '');
      }
    }
  }
}
