import { Component, Input } from '@angular/core';
import { Verse } from 'src/app/shared/models/book';

@Component({
  selector: 'bym-verse',
  templateUrl: './verse.component.html',
  styleUrls: ['./verse.component.scss']
})
export class VerseComponent {
  @Input() verse: Verse | undefined;
}
