import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BibleDataService } from 'src/app/shared/services/bible-data.service';
import { Verse } from 'src/app/shared/models/book';
import { ModalDialogComponent } from '../modal-dialog/modal-dialog.component';

@Component({
  selector: 'bym-chapter-content',
  templateUrl: './chapter-content.component.html',
  styleUrls: ['./chapter-content.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ChapterContentComponent implements OnInit, AfterViewInit{
  @Input() 
  set verses(verses: Array<Partial<Verse>> | undefined) {
    this.bibleDataService.resetCount();
    setTimeout(()=> this.listenNote(), 300);
    this._verses = verses;
  };

  get verses(): Array<Partial<Verse>> | undefined {
    return this._verses;
  }

  _verses: Array<Partial<Verse>> | undefined;

  constructor(private elem: ElementRef, private renderer: Renderer2, public dialog: MatDialog, private bibleDataService: BibleDataService) {}
  
  ngOnInit(): void {
    this.listenNote();
  }

  ngAfterViewInit() {
    this.listenNote();
  }

  verseNumber(osisID: string | undefined) {
    return osisID?.split('.')[2]
  }

  openDialog(note: string) {
    this.dialog.open(ModalDialogComponent, {
      data: {
        note
      },
      backdropClass: 'bym-note-overlay',
    });
  }

  listenNote() {
    const elements = this.elem.nativeElement.querySelectorAll('.foot-note');
    elements.forEach((element: any) => {
      this.renderer.listen(element, 'click', (event)=> {
        const note = event.target.getAttribute('data-note');
        this.openDialog(note);
      });
    });
  }
}
