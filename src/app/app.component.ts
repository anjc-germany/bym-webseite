import { ChangeDetectorRef, Component, OnInit } from '@angular/core';

import { MediaMatcher } from '@angular/cdk/layout';

import { Observable } from 'rxjs';
import { map, shareReplay, switchMap, tap } from 'rxjs/operators';

import { BibleDataService } from 'src/app/shared/services/bible-data.service';
import {
  SelectItem,
  Book,
  Osis_Header,
  Osis_Response,
} from 'src/app/shared/models/book';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  osis_books$: Observable<Book[]> | undefined;
  osis_header$: Observable<Osis_Header> | undefined;
  bibleVersions: SelectItem[] | undefined;
  langs: SelectItem[] = [{name: 'DE', lang: 'de'}, {name: 'FR', lang: 'fr'}];
  osisData$: Observable<Osis_Response> | undefined;
  title = 'bym-webseite';
  mobileQuery: MediaQueryList;
  navs = [
    { name: 'PAGES.BIBLE', path: '/bible' },
    { name: 'PAGES.VISION', path: '/vision' },
    { name: 'PAGES.DOWNLOAD', path: '/download' },
    { name: 'PAGES.CONTACT', path: '/contact' },
  ];
  private _mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private bibleDataService: BibleDataService,
    private translate: TranslateService
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.translate.setDefaultLang('de');
    this.translate.use('de');
  }

  ngOnInit(): void {
    this.bibleVersions = this.bibleDataService.bible_versions;
    this.osis_books$ = this.bibleDataService.bibleVersion$.pipe(
      switchMap((version_lang) =>
        this.bibleDataService.fetchOsisData(version_lang)
      ),
      map((data) => data.books.slice(39)),
      tap(data => console.log(data)),
      tap((data) => (this.bibleDataService.osis_books = [...data])),
      shareReplay()
    );

    // this.osis_header$ = osisData$.pipe(map((data) => data.header));
  }
}
