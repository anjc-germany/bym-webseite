import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatMenuModule} from '@angular/material/menu';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule} from '@angular/material/dialog';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTabsModule} from '@angular/material/tabs';

import { HeaderBarComponent } from './components/header-bar/header-bar.component';
import { FooterBarComponent } from './components/footer-bar/footer-bar.component';
import { BymSelectComponent } from './components/bym-select/bym-select.component';
import { BymInputComponent } from './components/bym-input/bym-input.component';
import { ChapterContentComponent } from './components/chapter-content/chapter-content.component';
import { BibleComponent } from './pages/bible/bible.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { DownloadComponent } from './pages/download/download.component';
import { ContactComponent } from './pages/contact/contact.component';
import { VisionComponent } from './pages/vision/vision.component';
import { FaqComponent } from './pages/faq/faq.component';
import { VerseComponent } from './components/verse/verse.component';
import { VersePipe } from './shared/pipes/verse.pipe';
import { ModalDialogComponent } from './components/modal-dialog/modal-dialog.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { SelectBookComponent } from './components/select-book/select-book.component';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    AppComponent,
    HeaderBarComponent,
    FooterBarComponent,
    BymSelectComponent,
    BymInputComponent,
    ChapterContentComponent,
    BibleComponent,
    PageNotFoundComponent,
    DownloadComponent,
    ContactComponent,
    VisionComponent,
    FaqComponent,
    VerseComponent,
    VersePipe,
    ModalDialogComponent,
    SearchBarComponent,
    SelectBookComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatSelectModule,
    MatMenuModule,
    MatInputModule,
    MatFormFieldModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatTabsModule,
    TranslateModule.forRoot({
      defaultLanguage: 'de',
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
    }
  })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
