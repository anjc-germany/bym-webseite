import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BibleComponent } from './pages/bible/bible.component';
import { ContactComponent } from './pages/contact/contact.component';
import { DownloadComponent } from './pages/download/download.component';
import { FaqComponent } from './pages/faq/faq.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { VisionComponent } from './pages/vision/vision.component';

const routes: Routes = [
  {path: 'bible', component: BibleComponent},
  {path: 'vision', component: VisionComponent},
  {path: 'faq', component: FaqComponent},
  {path: 'download', component: DownloadComponent},
  {path: 'contact', component: ContactComponent},
  { path: '',   redirectTo: '/bible', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
