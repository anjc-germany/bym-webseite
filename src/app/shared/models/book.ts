export interface Book {
    osisID: string;
    chapters: Array<Chapter>;
    introduction: string;
    title: string;
}

export type SearchResult = Pick<Book, "title"> & Pick<Chapter, "verses"> & {searchText: string};

export interface Osis_Header {
    work: Work;
}

export interface Work {
    osisWork: string;
    title: string;
    date: string;
    description: string;
    type: string;
    identifier: string;
    source: string;
    rights: string;
}

export interface Chapter {
    osisID: string;
    verses: Array<Partial<Verse>>;
}

export interface Verse {
    title: string;
    osisID: string;
    sID: string;
    text: string;
}

export interface Osis_Response  {
    books: Array<Book>;
    header: Osis_Header;
    osisRefWork: string;
    osisIDWork: string;
    'xml:lang': string;
}

export interface Data_Response {
    data: Osis_Response
}

export interface SelectItem {
    name: string;
    lang: string;
}