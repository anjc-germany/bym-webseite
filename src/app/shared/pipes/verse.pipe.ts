import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BibleDataService } from 'src/app/shared/services/bible-data.service';

@Pipe({
  name: 'versePipe'
})
export class VersePipe implements PipeTransform {
  constructor(private sanitized: DomSanitizer, private bibleDataService: BibleDataService) {}
  transform(verseText: string | undefined, ...args: unknown[]): unknown {
    const noteStart = "<note type='study'>";
    const noteEnd = "</note>"
    
    if(verseText?.includes(noteStart)) {
      this.bibleDataService.countNote();
      const noteStartPos = verseText.indexOf(noteStart);
      const noteEndPos = verseText.indexOf(noteEnd);
      const textBeforeNote = verseText.substring(0, noteStartPos);
      const textAfterNote = verseText.substring(noteEndPos + noteEnd.length);
      const noteText = verseText.substring(noteStartPos + noteStart.length, noteEndPos);

      const verseTransformed = `<span class="verse-text">
        ${textBeforeNote}
        <span class="foot-note" data-note="${noteText}">
          [${this.bibleDataService.getCountNote()}]
        </span>
        ${textAfterNote}
        </span>`;
      return this.sanitized.bypassSecurityTrustHtml(verseTransformed);
      // return verse
    }
    return verseText;
  }

}
