import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { Book, Verse } from '../models/book';
import { BibleDataService } from './bible-data.service';

@Injectable({ providedIn: 'root' })
export class SearchService {
  searchResultsSubject$ = new ReplaySubject<Partial<Verse>[]>(1);
  searchResults$ = this.searchResultsSubject$.asObservable();

  constructor(private bibleDataService: BibleDataService) {}

  search(searchText: string): Partial<Verse>[] {
    console.log('searching...');
    if (searchText === '') {
      console.log('no searchtext...');
      return [];
    }

    const searchTextRegExp = new RegExp(`${searchText}`, 'g');
    const result: Partial<Verse>[] = [];
    const noteRegExp = /<note [^>]+>(.*?)<\/note>/g;
    const books = [...this.bibleDataService.osis_books];
    books?.forEach((book) =>
      book.chapters.forEach((chapter) => {
        const chap = Object.assign({}, chapter);
        const foundVerses = chap.verses
          .map((verse) => {
            if (noteRegExp.test(verse.text ?? '')) {
              verse.text = verse?.text?.replace(noteRegExp, '');
            }
            if (verse.text?.match(searchTextRegExp)) {
              verse = {
                ...verse,
                text: verse?.text?.replace(searchTextRegExp, (match) => {
                  return '<span class="highlight">' + match + '</span>';
                }),
              };
            }
            return verse;
          })
          .filter((verse) => verse?.text?.match(searchTextRegExp));
        if (foundVerses.length > 0) {
          result.push(...foundVerses);
          return foundVerses;
        }
        return;
      })
    );

    return result;
  }

  searchChanged(searchResults: Partial<Verse>[]) {
    this.searchResultsSubject$.next(searchResults);
  }
}
