import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { SelectItem, Book, Chapter, Data_Response, Osis_Response } from '../models/book';

@Injectable({providedIn: 'root'})
export class BibleDataService {    
    readonly bible_versions: SelectItem[] = [{name: 'BYM-FR', lang: 'fr'}, {name: 'BYM-DE', lang: 'de'}];
    osis_books: Book[] = [];
    private chapterSubject$ = new ReplaySubject<Chapter>(1);
    public chapter$ = this.chapterSubject$.asObservable();
    private selectedBookSubject$ = new ReplaySubject<Book>(1);
    public selectedBook$ = this.selectedBookSubject$.asObservable();
    private bibleVersionSubject$ = new BehaviorSubject<string>('de');
    public bibleVersion$ = this.bibleVersionSubject$.asObservable();
    private chapterOsisIdSubject$ = new ReplaySubject<string | undefined>(1);
    public chapterOsisId$ = this.chapterOsisIdSubject$.asObservable();
    private _noteCount = 0;
 
    constructor(private httpClient: HttpClient) { 
    }

    fetchOsisData(lang: string): Observable<Osis_Response> {
        return this.httpClient.get<Data_Response>(`/assets/bible-${lang}.json`)
        .pipe(
            map(res => res.data)
        )
    }

    chapterSelected(chapter: Chapter) {
        this.chapterSubject$.next(chapter);
    }

    chapterOsisIdChanged(osisID: string | undefined) {
        this.chapterOsisIdSubject$.next(osisID);
    }

    bookSelected(book: Book) {
        this.selectedBookSubject$.next(book);
    }

    bibleVersionChanged(version: string) {
        console.log('bibleVersion:-', version);
        
        this.bibleVersionSubject$.next(version)
    }

    getCountNote(): number {
        return this._noteCount;
    }

    countNote() {
        this._noteCount ++;
    }

    resetCount() {
        this._noteCount = 0;
    }
}