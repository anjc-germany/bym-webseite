import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BibleDataService } from 'src/app/shared/services/bible-data.service';
import { Book, Chapter, Verse } from 'src/app/shared/models/book';
import { SearchService } from 'src/app/shared/services/search.service';

@Component({
  selector: 'bym-bible',
  templateUrl: './bible.component.html',
  styleUrls: ['./bible.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BibleComponent implements OnInit, OnDestroy {
  currentIndex = 0;
  selectedChapter: Chapter | undefined;
  selectedBook: Book | undefined;
  bookTitleWithChapter: string | undefined;
  chapterNr: string | undefined;
  unsubscribe = new Subject<void>();
  searchResults: Partial<Verse>[] = [];

  constructor(private bibleDataService: BibleDataService, private SearchService: SearchService) {}

  ngOnInit(): void {
    this.SearchService.searchResults$
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(sResult => {
        this.searchResults = sResult;
      });

    this.bibleDataService.selectedBook$
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((book) => {
        this.selectedBook = book;
      });
    this.bibleDataService.chapter$
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((chapter) => {
        this.selectedChapter = chapter;
        this.updatebookTitleWithChapter(chapter);
        this.currentIndex = parseInt(chapter?.osisID.split('.')[1]) - 1;
      });
  }

  nextChapter() {
    this.currentIndex++;
    this.selectedChapter = this.selectedBook?.chapters[this.currentIndex];
    this.updatebookTitleWithChapter(this.selectedChapter);
    this.bibleDataService.chapterOsisIdChanged(this.selectedChapter?.osisID);
    // this.chapter$ = this.bibleDataService.selectedBook$?.pipe(
    //   map((book) => book.chapters[this.currentIndex]),
    //   tap((chap) => {
    //     this.updatebookTitleWithChapter(chap);
    //     this.bibleDataService.chapterOsisIdChanged(chap.osisID);
    //   })
    // );
  }

  previousChapter() {
    this.currentIndex--;
    this.selectedChapter = this.selectedBook?.chapters[this.currentIndex];
    this.updatebookTitleWithChapter(this.selectedChapter);
    this.bibleDataService.chapterOsisIdChanged(this.selectedChapter?.osisID);
    // this.chapter$ = this.bibleDataService.selectedBook$?.pipe(
    //   map((book) => book.chapters[this.currentIndex]),
    //   tap((chap) => {
    //     this.updatebookTitleWithChapter(chap);
    //     this.bibleDataService.chapterOsisIdChanged(chap.osisID);
    //   })
    // );
  }

  updatebookTitleWithChapter(chapter: Chapter | undefined) {
    setTimeout(()=> {
      this.chapterNr = chapter?.osisID.split('.')[1];
      this.bookTitleWithChapter = `${this.selectedBook?.title} ${this.chapterNr}`;
    })
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
